<?php

namespace Application\Classes;

use Zend\View\Model\ViewModel;
/**
 * Description of ZF2CRUD
 * @author Robson
 */
class ZF2CRUD {
    public $view;
    public function __construct() {
        $this->view = new ViewModel(array('crud'=> 'Bem vindo ao CRUD!'));
        $this->view->setTemplate('application/index/teste.phtml');
    }
}
