<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Classes\ZF2CRUD;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $crud = new ZF2CRUD();
        $crud->view->setTemplate('application/index/teste.phtml');
        
        return $crud->view;
    }
}
